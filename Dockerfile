FROM node:lts-alpine

WORKDIR /usr/src

COPY package.json /usr/src/package.json

RUN npm install

COPY src/app.js /usr/src/src/
COPY src/app.test.js /usr/src/src/

USER nobody

EXPOSE 5000
CMD ["node", "src/app.js"]
