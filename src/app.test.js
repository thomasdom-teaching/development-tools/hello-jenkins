const request = require('supertest');
const app = require('./app.js');

describe('GET /', function() {
    it('responds with Hello Jenkins', function(done) {
        request(app).get('/').expect('Hello Jenkins!', done);
    });

    it('responds with Hello Toto', function(done) {
        request(app).get('/').expect('Hello Toto!', done);
    });
});
