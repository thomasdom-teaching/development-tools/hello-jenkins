install:
    npm install

build: install

dev:
    npm run dev

start:
    npm run start

analysis: build
    npm run lint

test: build
    npm run test --coverage

.PHONY: install build dev start analysis test
